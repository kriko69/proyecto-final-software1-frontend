import { Component } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  public showComponent:boolean=false;
  constructor(public router:Router) {
    
   }
   ngOnInit() {
    console.log(window.location.href);
    
    if (sessionStorage.getItem('token')==null) {
      this.showComponent = false;
    }else{
      this.showComponent = true;
    }
  }
}
