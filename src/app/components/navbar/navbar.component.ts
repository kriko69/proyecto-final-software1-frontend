import { Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { LavarelApiService } from 'app/services/lavarel-api.service';
import Swal  from 'sweetalert2';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {

  constructor(public route:Router,public servicio:LavarelApiService) { 
    
  }

  ngOnInit() {
  }
  
  logout()
  {
    sessionStorage.removeItem('Token-Verificacion');
    sessionStorage.removeItem('Token-Refresh');
    sessionStorage.removeItem('Identity');
    this.route.navigate(['/login']);
  }

  logs()
  {
    this.servicio.enviarLogs().subscribe(
      data=>{
        console.log(data);
        if(data['mensaje']=='enviado')
        {
          Swal.fire(
            'Exito',
            'Se envio un correo con los logs.',
            'success'
          )
        }else{
          Swal.fire(
            'Error',
            'No se envio un correo con los logs.',
            'error'
          )
        }
      },(error)=>{
        console.log('no hay DB');
        
      }
    );
  }

}
