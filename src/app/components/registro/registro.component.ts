import { Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import Swal  from 'sweetalert2';
import { LavarelApiService } from 'app/services/lavarel-api.service';

@Component({
  selector: 'app-registro',
  templateUrl: './registro.component.html',
  styleUrls: ['./registro.component.css']
})
export class RegistroComponent implements OnInit {

  form:FormGroup;
  constructor(public servicio:LavarelApiService,public route:Router) {
    this.form= new FormGroup({
      'carrera' : new FormControl('',[Validators.required]),
      'ci' : new FormControl('',[Validators.required]),
      'egreso' : new FormControl('',[Validators.required]),
      'nombre' : new FormControl('',[Validators.required]),
      'apellidos' : new FormControl('',[Validators.required]),
      'email' : new FormControl('',[Validators.required]),
      'nacimiento' : new FormControl('',[Validators.required]),
      'sexo' : new FormControl('',[Validators.required]),
      'telefono' : new FormControl('',[Validators.required]),
      'celular' : new FormControl('',[Validators.required]),
      'ciudad' : new FormControl('',[Validators.required]),
      'civil' : new FormControl('',[Validators.required]),
      'hijos' : new FormControl('',[Validators.required]),
      'pass1' : new FormControl('',[Validators.required]),
      'pass2' : new FormControl('',[Validators.required])
    });
   }

  ngOnInit() {
  }

  registrar()
  {
    console.log(this.form.value);
    if(this.form.value.pass1!=this.form.value.pass2)
    {
      Swal.fire(
        'Error en las password',
        'Las passwords no son las mismas',
        'error'
      );
    }
    else{
      if(!this.form.valid){
        Swal.fire(
          'Error en el formulario',
          'Complete el formulario correctamente',
          'error'
        );
      }else{
        this.servicio.registrar(this.form).subscribe(
          data=>{
            if(data['mensaje']=='persona y usuario creado con exito')
            {
              Swal.fire(
                'Exito',
                data['mensaje'],
                'success'
              );
              this.route.navigate(['/login']);
            }
            else{
              Swal.fire(
                'Algo sucedio',
                data['mensaje'],
                'warning'
              );
            }
          },(error)=>{
            console.log('no hay DB');
            
          }
        );
      }
    }
    
  }

  irLogin()
  {
    this.route.navigate(['/login']);
  }
}
