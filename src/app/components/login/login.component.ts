import { Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { FormGroup,FormControl,Validators } from '@angular/forms';
import { LavarelApiService } from '../../services/lavarel-api.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  form:FormGroup;
  mensajeError;
  Error=false;
  errorDB=false;
  existeErrorDB=false;
  constructor(public router:Router,private service:LavarelApiService) { 
    this.errorDB=false;
    this.form = new FormGroup({
      'ci': new FormControl('',[Validators.required]),
      'password' : new  FormControl('',[Validators.required])
    });
    
    
     
  }

  ngOnInit() {
  }

  acceder()
  {
    console.log(this.form.value);
    console.log(this.form.value.ci);
    let info=[];
    this.service.login(this.form).subscribe(
      (data)=>{
        info=Object.assign(data);
        
        
      },(error)=>{

        console.log('no hay DB');
        this.existeErrorDB=true;
        
        
      }
    );
    
    setTimeout(()=>{
      console.log('info',info);
      console.log(info['mensaje']);
      if(this.existeErrorDB)
      {
        this.errorDB=true;
      }else{
        if(info['mensaje']!='El usuario no existe.')
        {
          sessionStorage.setItem('Token-Verificacion',info['Token-Verificacion']);
          sessionStorage.setItem('Token-Refresh',info['Token-Refresh']);
          this.obtenerPayloadVerificacion();
          this.obtenerPayloadRefresh();
          this.router.navigate(['/usuarios']);
        }else
        {
          console.log(info['mensaje']);
          this.Error=true;
          this.mensajeError=info['mensaje'];
        }
      }
      
      
    },1000)
  }

  obtenerPayloadVerificacion()
  {
    this.service.verTokenVerificacion().subscribe(
      data=>{
        data =data.json()
        console.log(data);
        sessionStorage.setItem('Identity',JSON.stringify(data));
      },(error)=>{
        console.log('no hay DB');
        
      }
    );
  } 
  obtenerPayloadRefresh()
  {
    this.service.verTokenRefresh().subscribe(
      data=>{
        data =data.json()
        console.log(data);
        sessionStorage.setItem('IdentityRefresh',JSON.stringify(data));
      },(error)=>{
        console.log('no hay DB');
        
      }
    );
    
    
  }

}
