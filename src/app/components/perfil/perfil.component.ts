import { Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { LavarelApiService } from 'app/services/lavarel-api.service';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import Swal from'sweetalert2';
import { THIS_EXPR } from '@angular/compiler/src/output/output_ast';
@Component({
  selector: 'app-perfil',
  templateUrl: './perfil.component.html',
  styleUrls: ['./perfil.component.css']
})
export class PerfilComponent implements OnInit {

  perfil;
  form:FormGroup;
  editar;
  constructor(public servicio:LavarelApiService,public route:Router) {
    this.editar=true;
    this.servicio.verificarTokenParaRequest();
    setTimeout(() => {
      this.servicio.verPerfil().subscribe(
        data=>{
          data=data.json();
          console.log(data);
          
          this.perfil=Object.assign(data);
          this.form = new FormGroup({
            'nombre': new FormControl(this.perfil[0]['nombre'],[Validators.required]),
            'apellidos' : new  FormControl(this.perfil[0]['apellidos'],[Validators.required]),
            'ci' : new  FormControl(this.perfil[0]['ci'],[Validators.required]),
            'carrera' : new  FormControl(this.perfil[0]['carrera'],[Validators.required]),
            'email' : new  FormControl(this.perfil[0]['email'],[Validators.required]),
            'fecha' : new  FormControl(this.perfil[0]['fecha_nacimiento'],[Validators.required]),
            'telefono' : new  FormControl(this.perfil[0]['telefono'],[Validators.required]),
            'celular' : new  FormControl(this.perfil[0]['celular'],[Validators.required]),
            'ciudad' : new  FormControl(this.perfil[0]['ciudad'],[Validators.required]),
          });
          console.log(this.form.value);
          
        },(error)=>{
          console.log('no hay DB');
          
        }
      );
    }, 1000);
    
  }

  ngOnInit() {
  }

  editarValores()
  {
    console.log(this.form.value);
    this.servicio.verificarTokenParaRequest();

    setTimeout(() => {
      this.servicio.actualizarUsuario(this.form).subscribe(
        data=>{
          data=data.json();
          console.log(data);
          if(data['mensaje']=='El usuario se actualizo con exito')
          {
            Swal.fire(
              'Exito',
              'Los datos fueron actualizados',
              'success'
            );
            this.route.navigate(['/usuarios']);
          }else{
            Swal.fire(
              'Error',
              'Algo salio mal revise los datos',
              'error'
            );
          }
          
        },(error)=>{
          console.log('no hay DB');
          
        }
      );
    }, 1000);
    
  }
}
