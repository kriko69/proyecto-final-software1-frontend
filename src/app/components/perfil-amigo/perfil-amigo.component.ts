import { Component, OnInit } from '@angular/core';
import { LavarelApiService } from 'app/services/lavarel-api.service';
import { ActivatedRoute, Router } from '@angular/router';
import { FormGroup, FormControl, Validators } from '@angular/forms';

@Component({
  selector: 'app-perfil-amigo',
  templateUrl: './perfil-amigo.component.html',
  styleUrls: ['./perfil-amigo.component.css']
})
export class PerfilAmigoComponent implements OnInit {

  dataAmigo=[];
  form:FormGroup;
  constructor(public servicio:LavarelApiService,public active:ActivatedRoute,
    public route:Router) {
    let amigo_id;
    
    this.active.params.subscribe(
      data=>{        
        amigo_id=data['id']
      }
    );
    console.log(amigo_id);
    
    this.servicio.verificarTokenParaRequest();
    setTimeout(() => {
      this.servicio.verAmigo(amigo_id).subscribe(
        data=>{
          data=data.json();
          this.dataAmigo=Object.assign(data);
          console.log(data);
          this.form = new FormGroup({
            'nombre': new FormControl(this.dataAmigo[0]['nombre'],[Validators.required]),
            'apellidos' : new  FormControl(this.dataAmigo[0]['apellidos'],[Validators.required]),
            'ci' : new  FormControl(this.dataAmigo[0]['ci'],[Validators.required]),
            'carrera' : new  FormControl(this.dataAmigo[0]['carrera'],[Validators.required]),
            'email' : new  FormControl(this.dataAmigo[0]['email'],[Validators.required]),
            'fecha' : new  FormControl(this.dataAmigo[0]['fecha_nacimiento'],[Validators.required]),
            'telefono' : new  FormControl(this.dataAmigo[0]['telefono'],[Validators.required]),
            'celular' : new  FormControl(this.dataAmigo[0]['celular'],[Validators.required]),
            'ciudad' : new  FormControl(this.dataAmigo[0]['ciudad'],[Validators.required]),
          });
        },(error)=>{
          console.log('no hay DB');
          
        }
      );
    }, 1000);
   }

  ngOnInit() {
  }

  regresar()
  {
    this.route.navigate(['/amigos']);
  }

}
