import { Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { LavarelApiService } from 'app/services/lavarel-api.service';
import Swal from'sweetalert2';
@Component({
  selector: 'app-amigos',
  templateUrl: './amigos.component.html',
  styleUrls: ['./amigos.component.css']
})
export class AmigosComponent implements OnInit {

  nombre;
  amigos=[];
  copia_amigos=[];
  amigos_busqueda=[];
  filtro=false;
  tipoFiltro='todos';
  constructor(public servicio:LavarelApiService,public route:Router) { 
    this.listaramigos();
  }

  ngOnInit() {
  }

  hasta=[];
  paginaActual=1;
  listaramigos()
  {
    this.hasta=[];
    this.paginaActual=1;
    this.servicio.verificarTokenParaRequest();

    setTimeout(() => {
      this.servicio.listarAmigos(1).subscribe(
        data=>{
          data=data.json();
          console.log(data);
          this.amigos=Object.assign(data['data']);
          this.copia_amigos=Object.assign(data['data']);
          //this.amigos_busqueda=Object.assign(data.json());
          console.log(this.amigos);
          for (let i = 1; i <= data['last_page']; i++) {
            this.hasta.push(i);  
          }
        },(error)=>{
          console.log('no hay DB');
          
        }
      );
    }, 1000);
  }

  verUsuariosPaginador(pagina)
  {
    console.log('pag',pagina);
    this.paginaActual=pagina;
    this.servicio.verificarTokenParaRequest();
    setTimeout(() => {
      this.servicio.listarUsuarios(pagina).subscribe(
        data=>{
          data=data.json();
          console.log('usuarios',data);
          this.amigos=Object.assign(data['data']);
          this.copia_amigos=Object.assign(data['data']);
          /*this.copia_usuarios=Object.assign(data.json());
          this.usuarios_busqueda=Object.assign(data.json());*/
          console.log(this.amigos);
          
  
        },(error)=>{
          console.log('no hay DB');
          
        }
      );
    }, 1000);
    
  }
  flechasPaginadorAtras()
  {
    if(this.paginaActual==1)
    {
      console.log('no hay mas paginas');
    }else
    {
      this.paginaActual=this.paginaActual-1;
      this.servicio.verificarTokenParaRequest();
    setTimeout(() => {
      this.servicio.listarUsuarios(this.paginaActual-1).subscribe(
        data=>{
          data=data.json();
          console.log('usuarios',data);
          this.amigos=Object.assign(data['data']);
          this.copia_amigos=Object.assign(data['data']);
          /*this.copia_usuarios=Object.assign(data.json());
          this.usuarios_busqueda=Object.assign(data.json());*/
          console.log(this.amigos);
          
  
        },(error)=>{
          console.log('no hay DB');
          
        }
      );
    }, 1000);
    }
  }
  flechasPaginadorAdelante()
  {
    if(this.paginaActual==this.hasta.length)
    {
      console.log('no hay mas paginas');
    }else
    {
      this.paginaActual=this.paginaActual+1;
      this.servicio.verificarTokenParaRequest();
    setTimeout(() => {
      this.servicio.listarUsuarios(this.paginaActual+1).subscribe(
        data=>{
          data=data.json();
          console.log('usuarios',data);
          this.amigos=Object.assign(data['data']);
          this.copia_amigos=Object.assign(data['data']);
          /*this.usuarios_busqueda=Object.assign(data.json());*/
          console.log(this.amigos);
          
  
        },(error)=>{
          console.log('no hay DB');
          
        }
      );
    }, 1000);
    }
  }

  verAmigo(amigo_id)
  {
    this.route.navigate(['/amigo',amigo_id]);
  }
  eliminarAmigo(amigo_id)
  {
    console.log(JSON.parse(sessionStorage.getItem('Identity')).sub);
    let usuario_id=JSON.parse(sessionStorage.getItem('Identity')).sub;
    Swal.fire({
      title: 'Estas seguro?',
      text: 'Tu no podrias ser capaz de recuperar al amigo.',
      type: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Si, de acuerdo!',
      cancelButtonText: 'No, cancelar'
    }).then((result) => {
      if (result.value) {
        this.servicio.verificarTokenParaRequest();

        setTimeout(() => {
          this.servicio.eliminarAmigo(amigo_id).subscribe(
            data=>{
              data=data.json();
              console.log(data);
              
              if(data['mensaje']=="El amigo se elimino con exito")
              {
                Swal.fire(
                  'Eliminado!',
                  'El amigo fue eliminado con exito.',
                  'success'
                )
                this.listaramigos();
              }else{
                Swal.fire(
                  'Advertencia!',
                  data['mensaje'],
                  'info'
                )
              }
            },(error)=>{
              console.log('no hay DB');
              
            }
          );
        }, 1000);

        
        
      } else if (result.dismiss === Swal.DismissReason.cancel) {
        Swal.fire(
          'Cancelado',
          'La eliminacion fue cancelada',
          'error'
        )
      }
    })  
  }
  verFiltro(filtro)
  {
    if(filtro!='todos')
    {
      this.tipoFiltro=filtro;
      this.filtro=true;
    }else{
      this.amigos=this.copia_amigos;
      this.filtro=false;
    }
    
  }
  filtrar(palabra)
  {
    console.log(palabra+' '+this.tipoFiltro);
    if(this.tipoFiltro=='nombre')
    {
      this.filtrarPorNombre(palabra);
    }else{
      if(this.tipoFiltro=='apellido')
      {
        this.filtrarPorApellido(palabra);
      }else{
        if(this.tipoFiltro=='carnet')
        {
          this.filtrarPorCi(palabra);
        }
      }
    }
  }
  todosAmigos=[];
  obtenerTodosLosAmigos()
  {
    this.servicio.verificarTokenParaRequest();
    setTimeout(() => {
      this.servicio.listarAmigosTodos().subscribe(
        data=>{
          data=data.json();
          console.log(data);
          this.todosAmigos=Object.assign(data);
        }
      );
    }, 1000); 
  }
  filtrarPorNombre(palabra:string)
  {
    let nombre = palabra.toUpperCase();
    let resultados=[];
    this.todosAmigos=[];
    this.obtenerTodosLosAmigos();
    setTimeout(() => {
      for (let i = 0; i < this.copia_amigos.length; i++) {
        let nombre2:string=this.copia_amigos[i].nombre
        if(nombre2.toUpperCase().search(nombre)>-1)
        {
          resultados.push(this.copia_amigos[i]);
        }
      }
      this.amigos=resultados;
    }, 2000);
  }
  filtrarPorApellido(palabra:string)
  {
    let apellido = palabra.toUpperCase();
    let resultados=[];
    this.todosAmigos=[];
    this.obtenerTodosLosAmigos();
    setTimeout(() => {
      for (let i = 0; i < this.copia_amigos.length; i++) {
        let apellido2:string=this.copia_amigos[i].apellidos
        if(apellido2.toUpperCase().search(apellido)>-1)
        {
          resultados.push(this.copia_amigos[i]);
        }
      }
      this.amigos=resultados;
    }, 2000);
  }
  filtrarPorCi(carnet:number)
  {
    let resultados=[];
    this.todosAmigos=[];
    this.obtenerTodosLosAmigos();
    setTimeout(() => {
      for (let i = 0; i < this.copia_amigos.length; i++) {
        let carnet2=this.copia_amigos[i].ci
        if(carnet==carnet2)
        {
          resultados.push(this.copia_amigos[i]);
        }
      }
      this.amigos=resultados;
    }, 2000);
  }

}
