import { Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { LavarelApiService } from 'app/services/lavarel-api.service';
import Swal from'sweetalert2';
import { Observable } from "rxjs";
@Component({
  selector: 'app-usuarios',
  templateUrl: './usuarios.component.html',
  styleUrls: ['./usuarios.component.css']
})
export class UsuariosComponent implements OnInit {

  nombre;
  usuarios=[];
  copia_usuarios=[];
  usuarios_busqueda=[];
  AccesoRestringido=false;
  filtro=false;
  tipoFiltro='todos';
  tokenMensaje;
  
  constructor(public servicio:LavarelApiService,public route:Router) {
    
    
    //this.probarToken();

    setTimeout(() => {
      this.verPerfil();
      this.listarUsuarios();
    }, 1000);
    
   }

  ngOnInit() {
    
  }
  
  verPerfil()
  {
    this.servicio.verificarTokenParaRequest();
    setTimeout(() => {
      this.servicio.verPerfil().subscribe(
        data=>{
          data =data.json();
          console.log(data);
          this.nombre=data[0]['nombre']+' '+data[0]['apellidos'];
        },(error)=>{
          console.log('no hay DB');
          
        }
    );
    }, 1000);


  }
  
  hasta=[];
  paginaActual=1;
  listarUsuarios()
  {
    this.hasta=[];
    this.paginaActual=1;
    this.servicio.verificarTokenParaRequest();
    setTimeout(() => {
      this.servicio.listarUsuarios(1).subscribe(
        data=>{
          data=data.json();
          console.log('usuarios',data);
          this.usuarios=Object.assign(data['data']);
          this.copia_usuarios=Object.assign(data['data']);
          /*this.copia_usuarios=Object.assign(data.json());
          this.usuarios_busqueda=Object.assign(data.json());*/
          for (let i = 1; i <= data['last_page']; i++) {
            this.hasta.push(i);  
          }
          console.log(this.usuarios);
          if(this.usuarios['mensaje']!=undefined)
          {
            this.AccesoRestringido=true;
          }
  
        },(error)=>{
          console.log('no hay DB');
          
        }
      );
    }, 1000);
  }

  verUsuariosPaginador(pagina)
  {
    console.log('pag',pagina);
    this.paginaActual=pagina;
    this.servicio.verificarTokenParaRequest();
    setTimeout(() => {
      this.servicio.listarUsuarios(pagina).subscribe(
        data=>{
          data=data.json();
          console.log('usuarios',data);
          this.usuarios=Object.assign(data['data']);
          this.copia_usuarios=Object.assign(data['data']);
          /*this.copia_usuarios=Object.assign(data.json());
          this.usuarios_busqueda=Object.assign(data.json());*/
          console.log(this.usuarios);
          if(this.usuarios['mensaje']!=undefined)
          {
            this.AccesoRestringido=true;
          }
  
        },(error)=>{
          console.log('no hay DB');
          
        }
      );
    }, 1000);
    
  }
  flechasPaginadorAtras()
  {
    if(this.paginaActual==1)
    {
      console.log('no hay mas paginas');
    }else
    {
      this.paginaActual=this.paginaActual-1;
      this.servicio.verificarTokenParaRequest();
    setTimeout(() => {
      this.servicio.listarUsuarios(this.paginaActual-1).subscribe(
        data=>{
          data=data.json();
          console.log('usuarios',data);
          this.usuarios=Object.assign(data['data']);
          this.copia_usuarios=Object.assign(data['data']);
          /*this.copia_usuarios=Object.assign(data.json());
          this.usuarios_busqueda=Object.assign(data.json());*/
          console.log(this.usuarios);
          if(this.usuarios['mensaje']!=undefined)
          {
            this.AccesoRestringido=true;
          }
  
        },(error)=>{
          console.log('no hay DB');
          
        }
      );
    }, 1000);
    }
  }
  flechasPaginadorAdelante()
  {
    if(this.paginaActual==this.hasta.length)
    {
      console.log('no hay mas paginas');
    }else
    {
      this.paginaActual=this.paginaActual+1;
      this.servicio.verificarTokenParaRequest();
    setTimeout(() => {
      this.servicio.listarUsuarios(this.paginaActual+1).subscribe(
        data=>{
          data=data.json();
          console.log('usuarios',data);
          this.usuarios=Object.assign(data['data']);
          this.copia_usuarios=Object.assign(data['data']);
          /*this.usuarios_busqueda=Object.assign(data.json());*/
          console.log(this.usuarios);
          if(this.usuarios['mensaje']!=undefined)
          {
            this.AccesoRestringido=true;
          }
  
        },(error)=>{
          console.log('no hay DB');
          
        }
      );
    }, 1000);
    }
  }

  eliminarUsuario(usuario_id,nombre)
  {
    this.servicio.verificarTokenParaRequest();
    console.log(usuario_id);
    Swal.fire({
      title: 'Estas seguro?',
      text: 'Tu no podrias ser capaz de recuperar al usuario.',
      type: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Si, de acuerdo!',
      cancelButtonText: 'No, cancelar'
    }).then((result) => {
      if (result.value) {
        setTimeout(() => {
          this.servicio.eliminarUsuario(usuario_id).subscribe(
            data=>{
              data=data.json();
              console.log(data);
              
              if(data['mensaje']=="El usuario se elimino con exito")
              {
                Swal.fire(
                  'Eliminado!',
                  'El usuario '+nombre+' fue eliminado con exito.',
                  'success'
                )
                this.listarUsuarios();
              }else{
                Swal.fire(
                  'Advertencia!',
                  data['mensaje'],
                  'info'
                )
              }
            },(error)=>{
              console.log('no hay DB');
              
            }
          );
        }, 1000);

        
        
      } else if (result.dismiss === Swal.DismissReason.cancel) {
        Swal.fire(
          'Cancelado',
          'La eliminacion fue cancelada',
          'error'
        )
      }
    })
    
  }

  verFiltro(filtro)
  {
    if(filtro!='todos')
    {
      this.tipoFiltro=filtro;
      this.filtro=true;
    }else{
      this.usuarios=this.copia_usuarios;
      this.filtro=false;
    }
    
  }
  filtrar(palabra)
  {
    console.log(palabra+' '+this.tipoFiltro);
    if(this.tipoFiltro=='nombre')
    {
      this.filtrarPorNombre(palabra);
    }else{
      if(this.tipoFiltro=='apellido')
      {
        this.filtrarPorApellido(palabra);
      }else{
        if(this.tipoFiltro=='carnet')
        {
          this.filtrarPorCi(palabra);
        }
      }
    }
  }
  todosUsuario=[];
  obtenerTodosLosUsuarios()
  {
    this.servicio.verificarTokenParaRequest();
    setTimeout(() => {
      this.servicio.listarUsuariosTodos().subscribe(
        data=>{
          data=data.json();
          console.log(data);
          this.todosUsuario=Object.assign(data);
        }
      );
    }, 1000); 
  }
  filtrarPorNombre(palabra:string)
  {
    let nombre = palabra.toUpperCase();
    let resultados=[];
    this.todosUsuario=[];
    this.obtenerTodosLosUsuarios();
    setTimeout(() => {
      console.log('TODOS',this.todosUsuario);
      for (let i = 0; i < this.todosUsuario.length; i++) {
        let nombre2:string=this.todosUsuario[i].nombre;      
        if(nombre2.toUpperCase().search(nombre)>-1)
        {
          resultados.push(this.todosUsuario[i]);
        }
      }
      this.usuarios=resultados;
    }, 2000);
  }
  filtrarPorApellido(palabra:string)
  {
    let apellido = palabra.toUpperCase();
    let resultados=[];
    this.todosUsuario=[];
    this.obtenerTodosLosUsuarios();
    setTimeout(() => {
      for (let i = 0; i < this.todosUsuario.length; i++) {
        let apellido2:string=this.todosUsuario[i].apellidos;
        if(apellido2.toUpperCase().search(apellido)>-1)
        {
          resultados.push(this.todosUsuario[i]);
        }
      }
      this.usuarios=resultados;
    }, 2000);
  }
  filtrarPorCi(carnet:number)
  {
    let resultados=[];
    this.todosUsuario=[];
    this.obtenerTodosLosUsuarios();
    setTimeout(() => {
      for (let i = 0; i < this.todosUsuario.length; i++) {
        let carnet2=this.todosUsuario[i].ci;
        if(carnet==carnet2)
        {
          resultados.push(this.todosUsuario[i]);
        }
      }
      this.usuarios=resultados;
    }, 2000);
  }

}
