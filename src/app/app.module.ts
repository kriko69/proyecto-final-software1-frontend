import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

import { AppComponent } from './app.component';
import { LoginComponent } from './components/login/login.component';
import { NavbarComponent } from './components/navbar/navbar.component';
import { APP_ROUTING } from './app.routes';
import { RegistroComponent } from './components/registro/registro.component';
import { UsuariosComponent } from './components/usuarios/usuarios.component';
import { AmigosComponent } from './components/amigos/amigos.component';
import { LavarelApiService } from './services/lavarel-api.service';
import { HttpClientModule } from '@angular/common/http';
import { PerfilComponent } from './components/perfil/perfil.component';
import { PerfilAmigoComponent } from './components/perfil-amigo/perfil-amigo.component';
import { 
  AuthGuardService 
} from './services/auth-guard.service';
@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    NavbarComponent,
    RegistroComponent,
    UsuariosComponent,
    AmigosComponent,
    PerfilComponent,
    PerfilAmigoComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    APP_ROUTING,
    ReactiveFormsModule,
    HttpClientModule
  ],
  providers: [
    LavarelApiService,
    AuthGuardService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
