import { Router } from '@angular/router';
import { Injectable } from '@angular/core';
import { HttpClient,HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { FormGroup } from '@angular/forms';
import { Http, Headers } from '@angular/http';

@Injectable()
export class LavarelApiService {

  public token;
  public identity;
  public url='http://localhost:8000/api/';
  constructor(public http:HttpClient,public http2:Http,public route:Router) {
    
  }
  verificarTokenParaRequest()
  {
    let verificacionExp;
    let refreshExp;
    let d = new Date();
    let h= String(d.getTime());
    let compara = h.substr(0,9);
    let hora = parseInt(compara);

    if(sessionStorage.getItem('Identity')!=null)
    {
      verificacionExp=JSON.parse(sessionStorage.getItem('Identity')).exp;
      if(verificacionExp>=hora)
      {
        this.setToken('verificacion');
      }else{
        if(sessionStorage.getItem('IdentityRefresh')!=null)
        {
          refreshExp=JSON.parse(sessionStorage.getItem('IdentityRefresh')).exp;
          if(refreshExp>=hora)
          {
            //genero tokens
            this.probarToken(sessionStorage.getItem('Token-Refresh')).subscribe(
              data=>{
                data=data.json();
                sessionStorage.setItem('Token-Verificacion',data['Token-Verificacion']);
                sessionStorage.setItem('Token-Refresh',data['Token-Refresh']);
                this.obtenerPayloadVerificacion();
                this.obtenerPayloadRefresh();
              }
            );
            setTimeout(() => {
              this.setToken('verificacion');
            }, 1000);

          }else{
            //logueo
            console.log('necesita loguearse');
            this.route.navigate(['/login']);
          }
        }else{
          //logueo
          console.log('necesita loguearse');
          this.route.navigate(['/login']);
        }
      }
    }else{
      if(sessionStorage.getItem('IdentityRefresh')!=null)
        {
          refreshExp=JSON.parse(sessionStorage.getItem('IdentityRefresh')).exp;
          console.log('refreshExp',refreshExp);
          console.log('hora',hora);
          
          
          if(refreshExp>=hora)
          {
            //genero tokens
            console.log('tk refresh',sessionStorage.getItem('Token-Refresh'));
            
            this.probarToken(sessionStorage.getItem('Token-Refresh')).subscribe(
              data=>{
                data=data.json();
                sessionStorage.setItem('Token-Verificacion',data['Token-Verificacion']);
                sessionStorage.setItem('Token-Refresh',data['Token-Refresh']);
                this.obtenerPayloadVerificacion();
                this.obtenerPayloadRefresh();
              }
            );
            setTimeout(() => {
              this.setToken('verificacion');
            }, 1000);

          }else{
            //logueo
            console.log('necesita loguearse');
            this.route.navigate(['/login']);
          }
        }else{
          //logueo
          console.log('necesita loguearse');
          this.route.navigate(['/login']);
        }
    }

  }
  obtenerPayloadVerificacion()
  {
    this.verTokenVerificacion().subscribe(
      data=>{
        data =data.json()
        console.log(data);
        sessionStorage.setItem('Identity',JSON.stringify(data));
      },error=>{

      }
    );
  } 
  obtenerPayloadRefresh()
  {
    this.verTokenRefresh().subscribe(
      data=>{
        data =data.json()
        console.log(data);
        sessionStorage.setItem('IdentityRefresh',JSON.stringify(data));
      },error=>{

      }
    );
    
    
  }
  setToken(tipo)
  {
    if(tipo=='verificacion')
    {
      this.token=sessionStorage.getItem('Token-Verificacion');
    }else{
      if(tipo=='refresh')
      {
        this.token=sessionStorage.getItem('Token-Refresh');
      }
    }
  }

  registrar(form:FormGroup):Observable<any>
  {
    let data={
      
      "carrera":form.value.carrera,
      "ci":form.value.ci,
      "año_egreso":form.value.egreso,
      "nombre":form.value.nombre,
      "apellidos":form.value.apellidos,
      "email":form.value.email,
      "fecha_nacimiento":form.value.nacimiento,
      "sexo":form.value.sexo,
      "celular":form.value.celular,
      "telefono":form.value.telefono,
      "ciudad":form.value.ciudad,
      "estado_civil":form.value.civil,
      "hijos":form.value.hijos,
      "password":form.value.pass1
    };

    let header : any = new HttpHeaders({'Content-Type': 'application/json'}),
    opsi   : any = JSON.stringify(data);
    console.log('opsi',opsi);
    return this.http.post(''+this.url+'registrar', opsi, header);
  }
  login(form:FormGroup)
  {
    let data={
      "ci":form.value.ci,
      "password":form.value.password
    };

    let header : any = new HttpHeaders({'Content-Type': 'application/json','Authorization':'token'}),
    opsi   : any = JSON.stringify(data);
    return this.http.post(''+this.url+'login', opsi, header);
  }

  verTokenVerificacion()
  {
    let token=sessionStorage.getItem('Token-Verificacion');
    var headers = new Headers();
    headers.append("Authorization",token);
    return this.http2.get(''+this.url+'verToken',{headers:headers});
  }
  verTokenRefresh()
  {
    let token=sessionStorage.getItem('Token-Refresh');
    var headers = new Headers();
    headers.append("Authorization",token);
    return this.http2.get(''+this.url+'verToken',{headers:headers});
  }


  verPerfil()
  {
    //let token=sessionStorage.getItem('Token-Verificacion');
    //this.probarTokenRequest();
    var headers = new Headers();
    headers.append("Authorization",this.token);
    return this.http2.get(''+this.url+'perfil',{headers:headers});
  }

  listarUsuarios(pagina)
  {
    //this.probarTokenRequest();
    //let token=sessionStorage.getItem('Token-Verificacion');
    var headers = new Headers();
    headers.append("Authorization",this.token);
    return this.http2.get(''+this.url+'usuarios?page='+pagina,{headers:headers});
  }
  listarUsuariosTodos()
  {
    var headers = new Headers();
    headers.append("Authorization",this.token);
    return this.http2.get(''+this.url+'usuariosTodos',{headers:headers});
  }
  eliminarUsuario(usuario_id)
  {
    let token=sessionStorage.getItem('Token-Verificacion');
    var headers = new Headers();
    headers.append("Authorization",token);
    return this.http2.delete(''+this.url+'usuarios/eliminar/'+usuario_id,{headers:headers});
  }
  actualizarUsuario(form:FormGroup)
  {
    let token=sessionStorage.getItem('Token-Verificacion');
    var headers = new Headers();
    headers.append("Authorization",token);
    
    let data={
      "nombre": form.value.nombre,
      "apellidos": form.value.apellidos,
      "carrera": form.value.carrera,
      "fecha_nacimiento": form.value.fecha,
      "email": form.value.email,
      "telefono": form.value.telefono,
      "celular": form.value.celular
    };
    return this.http2.put(''+this.url+'perfil',JSON.stringify(data),{headers:headers});
  }

  listarAmigos(pagina)
  {
    let token=sessionStorage.getItem('Token-Verificacion');
    var headers = new Headers();
    headers.append("Authorization",token);
    return this.http2.get(''+this.url+'usuarios/misAmigos?page='+pagina,{headers:headers});
  }
  listarAmigosTodos()
  {
    let token=sessionStorage.getItem('Token-Verificacion');
    var headers = new Headers();
    headers.append("Authorization",token);
    return this.http2.get(''+this.url+'usuarios/misAmigosTodos',{headers:headers});
  }

  verAmigo(id)
  {
    let token=sessionStorage.getItem('Token-Verificacion');
    var headers = new Headers();
    headers.append("Authorization",token);
    return this.http2.get(''+this.url+'usuarios/misAmigos/'+id,{headers:headers});
  }
  eliminarAmigo(amigo_id)
  {
    let token=sessionStorage.getItem('Token-Verificacion');
    var headers = new Headers();
    headers.append("Authorization",token);
    return this.http2.delete(''+this.url+'usuarios/misAmigos/'+amigo_id,{headers:headers});
  }

  enviarLogs()
  {
    return this.http.get(''+this.url+'email');
  }

  probarToken(token)
  {
    //let token=sessionStorage.getItem('Token-Verificacion');
    var headers = new Headers();
    headers.append("Authorization",token);
    return this.http2.get(''+this.url+'probarToken',{headers:headers});
  }




  probarTokenRequest()
  {
    let token='verificacion';
    
    this.probarToken(sessionStorage.getItem('Token-Verificacion')).subscribe(
      data=>{
        data=data.json();
        if(data['mensaje']=='el token es null o no existe')
        {
          console.log('refresh');
          token='refresh';
        }
        if(data['mensaje']=='el token es incorrecto')
        {
          console.log('refresh')
          token='refresh';
        }
        if(data['mensaje']=='el token de verificacion ha expirado necesita el token de refresh')
        {
          console.log('refresh')
          token='refresh';
        }
        if(data['mensaje']=='el token de rerfresh ha expirado necesita loguearse de nuevo')
        {
          console.log('login');
          token='login';
          
        }
        if(data['mensaje']=='Enviar token verificacion')
        {
          console.log('verificacion');
          token='verificacion';
        }

      }
    );
    let pasoToken;
    setTimeout(() => {
      console.log('token',token);
      if(token=='verificacion')
      {
        pasoToken=sessionStorage.getItem('Token-Verificacion');
        console.log('pasotoken',pasoToken);
        this.setToken('verificacion');
      }else{
        if(token=='refresh')
        {
          if(sessionStorage.getItem('Token-Refresh')!=null)
          {
            pasoToken=sessionStorage.getItem('Token-Refresh');
            this.probarToken(pasoToken).subscribe(
              data=>{
                data=data.json();
                sessionStorage.setItem('Token-Verificacion',data['Token-Verificacion']);
                sessionStorage.setItem('Token-Refresh',data['Token-Refresh']);
                //this.obtenerPayload();
              }
            );
            setTimeout(() => {
              pasoToken=sessionStorage.getItem('Token-Verificacion');
              console.log('pasotoken',pasoToken);
              this.setToken('verificacion');
            }, 1000);
          }else{
            console.log('necesita loguearse');
            
            this.route.navigate(['/login']);
            
          }
        }else{
          if(token=='login')
          {
            console.log('necesita loguearse');
            this.route.navigate(['/login']);
          }
        }
      }
      
    }, 1000);
  }
}
