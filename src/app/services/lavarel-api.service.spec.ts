import { TestBed, inject } from '@angular/core/testing';

import { LavarelApiService } from './lavarel-api.service';

describe('LavarelApiService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [LavarelApiService]
    });
  });

  it('should ...', inject([LavarelApiService], (service: LavarelApiService) => {
    expect(service).toBeTruthy();
  }));
});
