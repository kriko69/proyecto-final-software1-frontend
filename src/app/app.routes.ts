import { RegistroComponent } from './components/registro/registro.component';
import { LoginComponent } from './components/login/login.component';
import { Routes, RouterModule } from '@angular/router';
import { UsuariosComponent } from './components/usuarios/usuarios.component';
import { AmigosComponent } from './components/amigos/amigos.component';
import { PerfilComponent } from './components/perfil/perfil.component';
import { PerfilAmigoComponent } from './components/perfil-amigo/perfil-amigo.component';
import { 
    AuthGuardService 
  } from './services/auth-guard.service';


const APP_ROUTER: Routes = [
    { path: 'login', component: LoginComponent},
    { path: 'registro', component: RegistroComponent},
    { path: 'usuarios', component: UsuariosComponent},
    { path: 'amigos', component: AmigosComponent},
    { path: 'perfil', component: PerfilComponent},
    { path: 'amigo/:id', component: PerfilAmigoComponent},
    { path: '**', redirectTo: '/login',  pathMatch: 'full' },
    { path: '', redirectTo: '/login', pathMatch: 'full' },

    //{ path: 'path/:routeParam', component: MyComponent },
    //{ path: 'staticPath', component: ... },
    //{ path: '**', component: ... },
    //{ path: 'oldPath', redirectTo: '/staticPath' },
    //{ path: ..., component: ..., data: { message: 'Custom' }
];


export const APP_ROUTING=RouterModule.forRoot(APP_ROUTER);
