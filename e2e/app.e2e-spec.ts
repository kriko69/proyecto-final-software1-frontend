import { RedAlumniFrontendPage } from './app.po';

describe('red-alumni-frontend App', () => {
  let page: RedAlumniFrontendPage;

  beforeEach(() => {
    page = new RedAlumniFrontendPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
